﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Classes;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Reflection;

namespace App
{
    class Program
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("iniciou..");

            Console.WriteLine("Digite uma ação: 1-Links | 2-Subtitle ");
            string input = Console.ReadLine();

            if (input == "1")
            {
                var links_ = new ILink<string>[]
                {
                    new Link<string>("a","b"),
                    new Link<string>("b","c"),
                    new Link<string>("c","b"),
                    new Link<string>("b","a"),
                    new Link<string>("c","d"),
                    new Link<string>("d","e"),
                    new Link<string>("d","a"),
                    new Link<string>("a","h"),
                    new Link<string>("h","g"),
                    new Link<string>("g","f"),
                    new Link<string>("f","e"),
                };

                List<string> list = Links.RoutesBetween("a", "e", links_);
                Console.WriteLine("Path found: " + String.Join("-", list.ToArray()));

            }
            else if (input == "2")
            {
                Subtitle.Shift(123);
            }
            else
            {
                Console.WriteLine("Opção inválida");
            }
            
            Console.ReadKey();
        }
    }
}
