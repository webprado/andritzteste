﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace App.Classes
{
    public class Subtitle
    {
        private static string path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
        private static string pathFile = path + "\\The.Matrix.1999.BluRay.720p.Malay.srt";
        private static string pathFileCopy = path + "\\The.Matrix.1999.BluRay.720p.Malay - Copy.srt";

        public static void Shift(int milliseconds, string input = "", string output = "")
        {
            if (input == "") input = pathFile;
            if (output == "") output = pathFileCopy;

            string line = "";
            string newLine = "";

            StreamReader file = new StreamReader(input);

            Console.WriteLine("\nIniciada a escrita no arquivo... Aguarde! \n");

            while ((line = file.ReadLine()) != null)
            {
                if (line.Contains("-->"))
                {
                    string[] timeBeginEndArray = Regex.Split(line, "-->");

                    DateTime timeBegin = ConvertSTRtoDateTime(timeBeginEndArray[0]).AddMilliseconds(milliseconds);
                    DateTime timeEnd = ConvertSTRtoDateTime(timeBeginEndArray[1]).AddMilliseconds(milliseconds);

                    line = timeBegin.Hour.ToString().PadLeft(2, '0') + ":" + timeBegin.Minute.ToString().PadLeft(2, '0') + ":" + timeBegin.Second.ToString().PadLeft(2, '0') + "," + timeBegin.Millisecond.ToString().PadLeft(3, '0') + " --> " +
                        timeEnd.Hour.ToString().PadLeft(2, '0') + ":" + timeEnd.Minute.ToString().PadLeft(2, '0') + ":" + timeEnd.Second.ToString().PadLeft(2, '0') + "," + timeEnd.Millisecond.ToString().PadLeft(3, '0');
                }

                newLine += line + "\n";
                //Console.WriteLine(newLine);                               
            }

            file.Close();

            File.WriteAllText(output, newLine);

            Console.WriteLine("\nArquivo gerado: \n" + output);
        }

        public static DateTime ConvertSTRtoDateTime(string HourMinuteMilliseconds)
        {
            string pattern = @"[0-9]{2}:[0-9]{2}:[0-9]{2}";
            Regex reg = new Regex(pattern, RegexOptions.IgnoreCase);
            Match match = reg.Match(HourMinuteMilliseconds);

            if (match.Success)
            {
                string[] tmp = match.Value.Split(':');

                string patternMili = @"[0-9]{3}";
                Regex regMili = new Regex(patternMili, RegexOptions.IgnoreCase);
                Match matchMili = regMili.Match(HourMinuteMilliseconds);
                if (matchMili.Success)
                {
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(tmp[0]), Convert.ToInt32(tmp[1]), Convert.ToInt32(tmp[2]), Convert.ToInt32(matchMili.Value));
                }
            }

            return DateTime.Now;
        }
    }
}
