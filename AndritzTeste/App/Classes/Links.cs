﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Classes
{
    public class Links
    {

        public static List<string> RoutesBetween(string sourceSearch, string targetSearch, ILink<string>[] listaLinks)
        {
            // a --> e

            List<string> listaReturn = new List<string>();

            string sourceAtual = "";
            string targetAtual = "";
            List<string> targetVisitado = new List<string>();
            List<string> sourceVisitado = new List<string>();

            foreach (var link_ in listaLinks)
            {
                if (sourceAtual == "" && targetAtual == "")
                {
                    listaReturn.Add(link_.Source);
                    listaReturn.Add(link_.Target);
                    sourceAtual = link_.Source;
                    targetAtual = link_.Target;

                    targetVisitado.Add(link_.Target);
                    sourceVisitado.Add(link_.Source);
                }
                else if (!sourceVisitado.Contains(link_.Target) && targetVisitado.Contains(link_.Source) && !listaReturn.Contains(link_.Target))
                {
                    listaReturn.Add(link_.Target);
                    targetVisitado.Add(link_.Target);
                }
                else
                {
                    continue;
                }
            }

            return listaReturn;
        }
    }
}
